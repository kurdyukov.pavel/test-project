import java.util.Optional;

public class O {
    private Optional<String> s;

    public Optional<String> getS() {
        return s;
    }

    public void setS(String s) {
        this.s = Optional.ofNullable(s);
    }
}
